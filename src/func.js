const getSum = (str1, str2) => {
  let num1 = Number(str1);
  let num2 = Number(str2);
  if(
    typeof str1 != "string" ||
    typeof str2 != "string" ||
    isNaN(num1) ||
    isNaN(num2)
  ){ 
    return false;
  }
  let result = num1 + num2;
  return result.toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  if(Array.isArray(listOfPosts)){
    let postCount = 0;
    let commentsCount = 0;
    for(const item of listOfPosts){
      if(item.author == authorName){
        postCount++;
      }
      let comments = item.comments;
      if(comments != undefined){
        comments.forEach((comment) => {
          if(comment.author == authorName){
            commentsCount++;
          }
        });
      }
    }
    return `Post:${postCount},comments:${commentsCount}`;
  }
};

const tickets=(people)=> {
  let cash = 25;
  for(let i = 1; i < people.length; i++){
    if(people[0] > 25){
      return "NO";
    }
    else if(people[i] > cash && Math.abs(people[i] - 25) > cash ){
      return "NO";
    }
    else if(people[i] == 25){
      cash += 25;
    }
    else{
      cash += people[i] - 25;
    }
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
